# Crude makefile for Attiny5 VGA project
# 
#  This is intended to be used with AVRA command-line assembler for AVR.
#   
# Change this to reflect your programmer
#
PROGRAMMER = -c usbasp

# Don't change below
#
#AVRAFLAGS = 
#GAVRASMFLAGS =
FUSES = -U fuse:w:0xfe:m
DEVICE = attiny5
AVRDUDE = avrdude $(PROGRAMMER) -p $(DEVICE)
COMPILE = avra $(AVRAFLAGS)
#COMPILE = gavrasm ($GAVRASMFLAGS)

# symbolic targets:
all:    pentaveega.hex

flash:  all
	$(AVRDUDE) -U flash:w:pentaveega.hex:i
 
fuse:
	$(AVRDUDE) $(FUSES)
 
install: fuse flash
 
clean:
	rm -f *.hex *.obj *.lst *.cof *~
 
pentaveega.hex: pentaveega.asm tn5def.inc
	$(COMPILE) -l pentaveega.lst pentaveega.asm

