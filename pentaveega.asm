;
; pentaveega.asm
;
; Simple demo for ATTiny5 6-pin MCU. Drives 8-color VGA out from 3 pins.
; Should also run nicely on ATTiny4, but I don't have the chip to test it.
;
; Author : Jari Tulilahti


.include "tn5def.inc"

.def temp	= r16
.def zero	= r17
.def vline_lo	= r18
.def vline_hi	= r19
.def pixel	= r20
.def counter	= r21
.def offset	= r22
.def one	= r23
.def font 	= r24
.def letter	= r25

; HSYNC macros
;
.macro hsync_on
	sbi DDRB, PB0
.endm

.macro hsync_off
	cbi DDRB, PB0
.endm

; VSYNC macros
;
.macro vsync_on
	sbi DDRB, PB2
.endm

.macro vsync_off
	cbi DDRB, PB2
.endm

; Mode macros
;
.macro mode_sync
	sbic PORTB, 0
	out DDRB, zero
	out PORTB, zero
.endm

.macro mode_pixel
	ldi temp, 0xFF
	out PORTB, temp
	out DDRB, zero
.endm

; ============
; Reset vector
; ============
;
.org 0x0000
	; Init one
	;
	ldi one, 1

	; Init rest :)
	;
	rjmp reset

; ==================
; Generic delay loop
; ==================
;
; This fits nicely inside unused vectors :)
;
delay:
	dec temp
	brne delay
	ret

; =======================================
; HSYNC routine. This is "the" interrupt.
; =======================================
;
; We should be at the END of horizontal line here,
; we fix the jitter caused by AVR instructions taking
; 1-3 clocks during front porch. Then we pulse the HSYNC,
; increase the vertical counter in registers and then
; check if we're in Vertical blanking area (and jump to
; it's own routine), and finally draw "pixels"
;
; (TIM0_COMPA vector)
;
.org OC0Aaddr
	; First, let's fix jitter
	;
	in temp, TCNT0L
jitterfix:
	ldi ZH, high(jitterfix)
	ldi ZL, low(jitterfix)
	add ZL, temp
	ijmp
	nop

	; Reset stack pointer
	;
	ldi temp, low(RAMEND)
	out SPL, temp

	; Sync mode, HSYNC on
	;
	mode_sync

	ldi temp, 2
front_porch:
	dec temp
	brne front_porch

	hsync_on

	; Sync pulse. Wait.
	;
	ldi temp, 12
	rcall delay

	; HSYNC off
	;
	hsync_off

	; Increase vertical counter, check for blanking area
	;
	add vline_lo, one
	adc vline_hi, zero

	; Check if we are in visible part of screen
	;
	sbrc vline_hi, 1		; Skip jump if bit 9 of vline is clear (<512)
	rjmp vertical_blank		; Jump if we're outside visible screen

	; Back porch
	;
	ldi temp, 2
	rcall delay

	; Visible screen - Draw here
	;
	mode_pixel

	; Background bars
	;
	out DDRB, pixel

	; Wait for more center of screen
	;
	ldi temp, 13
	rcall delay

	; Fetch curvy bar offset & wait
	;
	mov temp, counter
	inc counter
	lsr temp
	lsr temp
	lsr temp
	andi temp, 31
	ldi ZL, low((wave << 1) + 0x4000)
	ldi ZH, high((wave << 1) + 0x4000)
	add ZL, temp
	ld temp, Z
	rcall delay

	; Draw text
	;
	bst font, 3
	bld temp, 0
	bld temp, 2
	bld temp, 3
	out DDRB, zero
	out DDRB, temp
	bst font, 2
	bld temp, 0
	bld temp, 2
	bld temp, 3
	out DDRB, temp
	bst font, 1
	bld temp, 0
	bld temp, 2
	bld temp, 3
	out DDRB, temp
	bst font, 0
	bld temp, 0
	bld temp, 2
	bld temp, 3
	out DDRB, temp
	nop
	nop
	nop

	; Black border
	;
	out DDRB, zero

	; Color bars again
	;
	out DDRB, pixel

	; Wavy thingie
	;
	ldi temp, 14
	rcall delay

	; Colorful middle
	;
	mov temp, counter
	neg temp
	lsr temp
	lsr temp
	or temp, pixel
	out DDRB, zero
	out DDRB, temp

	; Bar width
	;
	ldi temp, 3
	rcall delay

	out DDRB, zero

	; Background bars
	;
	out DDRB, pixel

make_text:
	; Create text here
	;
	inc XL
	cpi XL, 4
	brne letters_done
	clr XL

	mov temp, letter
	lsr temp
	ldi ZL, low((texting << 1) + 0x4000)
	ldi ZH, high((texting << 1) + 0x4000)
	add ZL, temp
	ld font, Z

	sbrs letter, 0
	swap font

	inc letter
	cpi letter, 120
	brne letters_done
	clr letter

letters_done:

	; Calculate background stripes
	;
	clr pixel
	ldi ZL, low(SRAM_START)
	ldi ZH, high(SRAM_START)

	; Red bar
	;
	rcall stripe
	bld pixel, 0

	; Blue bar
	;
	rcall stripe
	bld pixel, 3

	; Colorful steady bar
	;
	rcall stripe
	brtc color_end
	mov pixel, counter
	lsr pixel
	lsr pixel
	lsr pixel

color_end:
	; Wait for next HSYNC
	;
	rjmp wait_hsync


; =================
; Calculate stripes
; =================
;
stripe:
	; Load stripe value and check if we need to
	; check if stripe starts
	;
	set
	ld YL, Z+
	ld YH, Z+
	sub YL, vline_lo
	sbc YH, vline_hi
	cpi YL, 40
	cpc YH, zero
	brlo stripe_return
	clt

stripe_return:
	ret 

; ==============
; Initialization
; ==============
;
reset:
	; Set up timer interrupt. My counter is not set to 381 (380) because
	; my oscillator is not exactly 12MHz but with 12MHz oscillator, 380
	; is correct value.
	;
	ldi temp, (1 << OCIE0A)			; Compare interrupt enable
	out TIMSK0, temp
	ldi temp, 0x01				; Set counter value.
	out OCR0AH, temp			; 12000000 / 381
	ldi temp, 0x82				; gives vertical refresh
	out OCR0AL, temp			; of about 31.5kHz
	ldi temp, (1 << WGM02) | (1 << CS00)	; CTC mode, prescaler clk/1
	out TCCR0B, temp

	; Set MCU to use external oscillator
	;
	ldi temp, 0xD8
	out CCP, temp
	ldi temp, 0x02
	out CLKMSR, temp
	ldi temp, 0xD8
	out CCP, temp
	out CLKPSR, zero

	; Init stripes
	;
	ldi ZL, low(SRAM_START)
	ldi ZH, high(SRAM_START)
	ldi temp, 100
	st Z+, temp
	st Z+, zero
	ldi temp, 160
	st Z+, temp
	st Z+, one
	ldi temp, 24
	st Z+, temp
	st Z+, one

	; Start up
	;
	rjmp wait_hsync

; ======================
; Vertical blanking area
; ======================
;
vertical_blank:
	; Vertical blanking area.
	; Check for VSYNC pulse begin
	;
	clr font

	cpi vline_lo, 11
	brne check_vp_end		; Nope
	vsync_on			; Yes, just flip VSYNC pin state
	rjmp wait_hsync			; Jump waiting HSYNC

check_vp_end:
	; Check VSYNC pulse end
	;
	cpi vline_lo, 13
	brne check_move_bars		; Not pulse end
	vsync_off			; Flip sync pin state
	rjmp wait_hsync			; Jump waiting HSYNC

check_move_bars:
	; Move bars
	;
	cpi vline_lo, 14
	brne check_screen_end		; No bar movement

	; Move red bar
	;
	lds YL, SRAM_START
	lds YH, SRAM_START + 1
	ldi temp, 2
	add YL, temp
	adc YH, zero
	ldi temp, 0x2D
	cp YL, temp
	ldi temp, 2
	cpc YH, temp
	brlt store_red
	ldi YL, 2
	ldi YH, 0
store_red:
	sts SRAM_START, YL
	sts SRAM_START + 1, YH

	; Move blue bar
	;
	lds YL, SRAM_START + 2
	lds YH, SRAM_START + 3
	sub YL, one
	sbc YH, zero
	cp YL, zero
	cpc YH, zero
	brne store_blue
	ldi YL, 0x20
	ldi YH, 2
store_blue:
	sts SRAM_START + 2, YL
	sts SRAM_START + 3, YH


	rjmp wait_hsync			; Jump waiting HSYNC


check_screen_end:
	cpi vline_lo, 45		; Check if we've done 525 lines
	brne wait_hsync			; Not yet, jump waiting HSYNC
	ldi vline_lo, 32		; Start from line 32 (512 - 480 = 32)
	clr vline_hi			; Clear high byte

	; Move curve
	;
	inc offset
	inc offset
	mov counter, offset
	clr letter
	clr XL


; HSYNC WAIT:
; - enable interrupts
; - wait for HSYNC interrupt
;
wait_hsync:
	sei

wait_isr:
	rjmp wait_isr

.org 0x00D2
texting:
	.db 0x00, 0xe9, 0xea, 0x90, 0x69, 0xf9, 0x90, 0x9a
	.db 0xca, 0x90, 0xf8, 0xe8, 0xf0, 0xe4, 0x44, 0x40
	.db 0xe4, 0x44, 0x40, 0x44, 0x44, 0x40, 0xe4, 0x44
	.db 0x40, 0x44, 0x44, 0x40, 0xf8, 0xe8, 0xf0, 0xe9
	.db 0x99, 0xe0, 0xf8, 0xe8, 0xf0, 0x00, 0x00, 0x00
	.db 0xe9, 0xea, 0x90, 0x69, 0x99, 0x60, 0x78, 0x88
	.db 0x70, 0x9a, 0xca, 0x90, 0xf8, 0xf1, 0xf0, 0x00 
	.db 0x09, 0x09, 0x60, 0x00

.org 0x00F0
wave:
	.db 0x01, 0x01, 0x02, 0x02, 0x03, 0x04, 0x06, 0x07
	.db 0x09, 0x0a, 0x0b, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c
	.db 0x0c, 0x0c, 0x0b, 0x0a, 0x09, 0x07, 0x06, 0x04
	.db 0x03, 0x02, 0x02, 0x01, 0x01, 0x01, 0x01, 0x01
