PentaVeega - Little sister of OctaPentaVeega.

8-Color VGA output from 6-pin tiny Microcontroller ATtiny5, which has 512 bytes of flash and 32 bytes of RAM available.

VGA is generated using 3 pins, leds acting as diodes and utilizing the fact that each pin actually has 3 states: pulled high, pulled low and floating.

Current code uses 492 bytes of the 512 bytes flash, displaying the following:

https://www.youtube.com/watch?v=WdBNR0JEDcY
